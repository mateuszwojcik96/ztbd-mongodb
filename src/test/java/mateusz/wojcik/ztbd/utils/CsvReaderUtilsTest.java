package mateusz.wojcik.ztbd.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CsvReaderUtilsTest {

    CsvReaderUtils csvReaderUtils;

    @BeforeEach
    void setUp(){
        this.csvReaderUtils = new CsvReaderUtils();
    }

    @Test
    void shouldReturnThousandRecordsFromCsvFile(){
        // Given
        String path = "C:\\Users\\mwojc\\Documents\\studia\\ZTBD\\sofia-air-quality-dataset\\2017-09_sds011sof.csv";

        // When
        int returnedSize = this.csvReaderUtils.read1000RecordsFromCsvFile(path).size();

        // Then
        assertEquals(1000, returnedSize);
    }

    @Test
    void shouldReturnMultithreadsThousandRecordsFromCsvFile() {
        // Given
        String path = "C:\\Users\\mwojc\\Documents\\studia\\ZTBD\\sofia-air-quality-dataset";

        // When
        int returnedSize = this.csvReaderUtils.readRecordsFromCsvFileMultiThreads(path).size();

        // Then
        assertEquals(1000, returnedSize);
    }

}
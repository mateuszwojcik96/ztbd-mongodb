package mateusz.wojcik.ztbd.controllers;

import mateusz.wojcik.ztbd.models.Performance;
import mateusz.wojcik.ztbd.models.Sds;
import mateusz.wojcik.ztbd.repository.PerformanceRepository;
import mateusz.wojcik.ztbd.repository.SdsRepository;
import mateusz.wojcik.ztbd.utils.CsvReaderUtils;
import org.apache.tomcat.util.threads.ThreadPoolExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
@RequestMapping("/api")
public class AirQualityController {

    private SdsRepository sdsRepository;
    private CsvReaderUtils csvReaderUtils;
    private PerformanceRepository performanceRepository;

    public AirQualityController(SdsRepository sdsRepository, PerformanceRepository performanceRepository) {
        this.sdsRepository = sdsRepository;
        this.csvReaderUtils = new CsvReaderUtils();
        this.performanceRepository = performanceRepository;
    }

    @PostMapping("/add/sds")
    public String addSds() throws IOException {
        String path = "C:\\Users\\mwojc\\Documents\\studia\\ZTBD\\sofia-air-quality-dataset\\2017-11_sds011sof.csv";
        List<Sds> sdsList = csvReaderUtils.readRecordsFromCsvFileMultiThreads(path);


        Runtime runtime = Runtime.getRuntime();

        NumberFormat format = NumberFormat.getInstance();

        ExecutorService executorService = Executors.newFixedThreadPool(8);
        for(int i = 0; i< sdsList.size(); i ++){
            int finalI = i;

            executorService.execute(()->{
                sdsRepository.save(sdsList.get(finalI));
//                performanceRepository.save(new Performance(finalI, (runtime.freeMemory() / 1024)));
            });
        }

        executorService.shutdown();
        return "OK";
    }

    @GetMapping("/find")
    public ResponseEntity<?> findById(@RequestParam String id) {
        return new ResponseEntity<>(sdsRepository.findAllBySensorId(id), HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<?> editById(@RequestParam String id, @RequestParam String newP1) {

        ExecutorService executorService = Executors.newFixedThreadPool(8);

        for (Sds sds:
                sdsRepository.findAllBySensorId(id)) {
            executorService.execute(()->{
                sds.setP1(newP1);
                sdsRepository.save(sds);
            });
        }

        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> deleteById(@RequestParam String id) {
        sdsRepository.deleteBySensorId(id);

        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

}

package mateusz.wojcik.ztbd.utils;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import mateusz.wojcik.ztbd.models.Sds;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Slf4j
public class CsvReaderUtils {
    public List<Sds> read1000RecordsFromCsvFile(String filePath) {
        try {
            CsvToBean<Sds> sdsCsvToBean = readCsvFile(filePath);
            List<Sds> mappedObjectList = new ArrayList<>();
            sdsCsvToBean.iterator().next();

            do {
                mappedObjectList.add(sdsCsvToBean.iterator().next());
            } while (sdsCsvToBean.iterator().hasNext() && mappedObjectList.size() < 1000);

            return mappedObjectList;
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    public CsvToBean<Sds> readCsvFile(String filePath) throws IOException {
        Reader reader = Files.newBufferedReader(Paths.get(filePath));
        return new CsvToBeanBuilder(reader)
                .withType(Sds.class)
                .withIgnoreLeadingWhiteSpace(true)
                .build();
    }

    public List<Sds> readRecordsFromCsvFileMultiThreads(String filePath) {

        List<Sds> sds = new ArrayList<>();
        try {
            
            File inputF = new File(filePath);
            InputStream inputStream = new FileInputStream(inputF);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            sds = bufferedReader.lines().skip(1).map(mapToItem).collect(Collectors.toList());
            bufferedReader.close();
            return sds;

        } catch (Exception e){
            return sds;
        }
    }

    private Function<String, Sds> mapToItem = (line) -> {
        String[] p = line.split(",");
        Sds item;
        try {
        item = new Sds(p[0], p[1], p[2], p[3], p[4] , p[5], p[6], p[7]);
        } catch (Exception e) {
            item = new Sds(p[0], p[1], p[2], p[3], p[4] , p[5], "", "");
        }
        return item;
    };

}

package mateusz.wojcik.ztbd.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class Sds {
    private String id;
    private String id2;
    private String sensorId;
    private String location;
    private String lat;
    private String lon;
    private String timestamp;
    private String p1;
    private String p2;

    public Sds(String id2, String sensorId, String location, String lat, String lon, String timestamp, String p1, String p2) {
        this.id2 = id2;
        this.sensorId = sensorId;
        this.location = location;
        this.lat = lat;
        this.lon = lon;
        this.timestamp = timestamp;
        this.p1 = p1;
        this.p2 = p2;
    }
}

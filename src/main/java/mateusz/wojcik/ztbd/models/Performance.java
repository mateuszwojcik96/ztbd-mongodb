package mateusz.wojcik.ztbd.models;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Performance {
    private int iteration;
    private long memoryUsage;
}

package mateusz.wojcik.ztbd.repository;

import mateusz.wojcik.ztbd.models.Performance;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PerformanceRepository extends MongoRepository<Performance, String> {
}

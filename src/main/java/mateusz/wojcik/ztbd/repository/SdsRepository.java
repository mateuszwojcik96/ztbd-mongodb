package mateusz.wojcik.ztbd.repository;

import mateusz.wojcik.ztbd.models.Sds;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SdsRepository extends MongoRepository<Sds, String> {
    List<Sds> findAllBySensorId(String sensorId);
    void deleteBySensorId(String id);
}

